package com.ejung;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MyApp {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private int callCnt =0;
	private int callRandomCnt =0;
	
    @Autowired
    RestTemplateBuilder restTemplateBuilder;
    
	@Value("${version}")
	private String version;
	
	@Value("${serverHost}")
	private String serverHost;
	
	
    @ResponseBody
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(@RequestParam("file") MultipartFile multipartFile) {
    	logger.info("### uploaded : {}" + multipartFile.getOriginalFilename());
      File targetFile = new File("/tmp/3scale-file-receiver-" + multipartFile.getOriginalFilename());
      try {
        InputStream fileStream = multipartFile.getInputStream();
        FileUtils.copyInputStreamToFile(fileStream, targetFile);
      } catch (IOException e) {
        FileUtils.deleteQuietly(targetFile);
        logger.info("Fail to save the uploaded file.");
      }
      return "OK";
    }

    @RequestMapping(value = "/uploadMulti", method = RequestMethod.POST)
    public String uploadMulti(@RequestParam("file") List<MultipartFile> multipartFiles) {
    	for (MultipartFile multipartFile : multipartFiles) {
    	      File targetFile = new File("/tmp/3scale-file-receiver-" + multipartFile.getOriginalFilename());
    	      try {
    	        InputStream fileStream = multipartFile.getInputStream();
    	        FileUtils.copyInputStreamToFile(fileStream, targetFile);
    	      } catch (IOException e) {
    	        FileUtils.deleteQuietly(targetFile);
    	        logger.info("Fail to save the uploaded file.");
    	      }

    	}
      return "OK";
    }
    
    @RequestMapping("/requestHeader")
    @ResponseBody
    public Message displayMessage(@RequestHeader(value="Custom-Request") String customRequest) {
    	logger.info("customRequest: " + customRequest);
        return new Message("customRequest" + customRequest);
    }

    @RequestMapping("/responseHeader")
    @ResponseBody
    public ResponseEntity<String> displayHeaderMessage(@RequestHeader(value="Custom-Request") String customRequest) {

    	logger.info("customRequest: " + customRequest);
        SimpleDateFormat format1 = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss");

        HttpHeaders headers = new HttpHeaders();
        headers.add("custom-Response-Time", format1.format(new Date()));
        return ResponseEntity.ok()
                           .headers(headers)
                           .body("SUCCESS");
    }

    @RequestMapping(value = "/return_404", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity sendViaResponse404() {
    	logger.info("===== return_404 =====");
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(value = "/return_406", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity sendViaResponse406() {
    	logger.info("===== return_406 =====");
        return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    }
    
    @RequestMapping(value = "/return_502", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity sendViaResponse502() {
    	logger.info("===== return_502 =====");
        return new ResponseEntity(HttpStatus.BAD_GATEWAY);
    }
 
    @RequestMapping(value = "/return_503", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity sendViaResponse503() {
    	logger.info("[GET] call return_503 call Count : " + callCnt);
    	callCnt++;
    	//System.out.println("===== return_503 =====");
        return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
    }

    @RequestMapping(value = "/randomError", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> sendRandomError503() {
    	logger.info("[GET] call ramdomError call Count : " + callRandomCnt); 
    	//System.out.println("===== randomError =====");
    	

        int idx = new Random().nextInt(3);
        
        logger.info("[ RandomNum: " +idx+"]");
        logger.info("[ Version: " +version+"]");
        callRandomCnt++;
        if(idx == 1 && version.equalsIgnoreCase("v2")) {
        	return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[GET] /randomError Return Code : 503 [Version: "+version+"]" );
//        	return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE).;
        }else {
        	return ResponseEntity.status(HttpStatus.OK).body("[GET] /randomError Return Code : 200 [Version: "+version+"]");
        }
    }
    
    @RequestMapping(value = "/greetings", method = RequestMethod.GET)
    @ResponseBody
    public Message displayMessage() {
    	logger.info("===== greetings =====");
        return new Message("Greetings!");
    }
    
    @RequestMapping(value = "/version", method = RequestMethod.GET)
    @ResponseBody
    public Message getVersion() {
    	logger.info("logger.info verion");
    	//System.out.println("===== called [GET] version =====");
    	    	
        return new Message("version: "+version);
    }
/*     @RequestMapping("/")  
    public String index() {
       //has to be without blank spaces
       return "forward:index.html";
    }     */
    
    @RequestMapping(value = "/healthz", method = RequestMethod.GET)
    public String hi() throws InterruptedException {
    	System.out.println("===== called [GET] healthz =====");
    	RestTemplate restTemplate = restTemplateBuilder.build();
        //Thread.sleep(3000L);
        String hiResult = restTemplate.getForObject("http://"+serverHost+"/spring-api/responseHealthz", String.class);
        return hiResult;
    }
    
	@RequestMapping(value = "/responseHealthz", method = RequestMethod.GET)
    //@GetMapping("/bye")
    public String bye() throws InterruptedException {
		
		System.out.println("===== called [GET] responseHealthz =====");
        return "GOOD";
    }

    static class Message {
        private String content = "";
        
        Message (String content) {
        	this.content = content;
        }
        
        public String getContent() {
            return content;
        }
    }
}